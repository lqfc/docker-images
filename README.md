# docker-images

```sh
$ docker login registry.gitlab.com
```

## ta-lib

```sh
$ cd ta-lib
$ docker build -t registry.gitlab.com/lqfc/docker-images/ta-lib .
$ docker push registry.gitlab.com/lqfc/docker-images/ta-lib
```

## trading-bot

```sh
$ cd trading-bot
$ docker build -t registry.gitlab.com/lqfc/docker-images/trading-bot .
$ docker push registry.gitlab.com/lqfc/docker-images/trading-bot
```
